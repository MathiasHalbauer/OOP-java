package rauschen;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class IO
{

    /**
     * Path of the file that the class is going to read
     */
    private String inputPath;

    /**
     * Path of the file, that should contain the results of (de)noise-process.
     */
    private String outputPath;

    /**
     * Saving the signs of the inputfile in a list for further processing.
     */
    private List<String> tokenlist;


    /**
     * Constructor for testing reading the file
     * @param console consists of user input by the input-class.
     */
    public IO(Input console)
    {
        List<Character> charList = readSingleCharacter(console.getInputPath());
        this.outputPath =console.getOutputPath();

            tokenlist = characterToString(charList);





    }

    /**
     * Constructor that is reading the scrambled message and writing the file. Uses the results of the noise class.
     * @param console Userinput in the class Input.
     * @param noisyMessage List of strings that
     */
    public IO(Input console, List<String> noisyMessage)
    {
        this.inputPath=console.getInputPath();
        this.outputPath=console.getOutputPath();

        String temp = cutToString(noisyMessage);

        writeOutput(outputPath, temp);


    }

    /**
     * Method to reach each char of the message by using a BufferedReader that reads each char of the message.
     * @param myFile File that should be read.
     * @return List of each character of the message.
     */

    public List<Character> readSingleCharacter(String myFile)
    {
        ArrayList<Character> list1 = new ArrayList<Character>() ;

        //Nutzung von try with Ressources, um den Buffered Reader automatisch zu schließen

        try(BufferedReader  myBR = new BufferedReader(new FileReader(myFile)))
        {
            checkForEmptyFile(myFile);
            int x;

            while (  (x=myBR.read()) != -1   )
            {

                    list1.add((char) x);

            }

        }

        catch (FileNotFoundException f)
        {
            f.printStackTrace();

        }

        catch( IOException e)
        {
            e.printStackTrace();
            System.out.println("Programm wird abgebrochen\n");
        }
        return list1;

    }

    /**
     * Method to write a string (result of the noise process) into a text file. Uses af FileWriter wrapped into a BufferedWriter to write the content into the file.
     * @param file File into which the content should be written. If the file does not already exist, it will be automatically created by the FileWriter
     * @param content content for the file to be created. Consists of a string.
     */

    public void writeOutput(String file, String content)
    {

        try( BufferedWriter bw = new BufferedWriter( new FileWriter (file) ) )
        {
            bw.write(content);
        }

        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (Exception f)
        {
            f.printStackTrace();
        }

    }

    /**
     * Constructs a single string out of a list of strings.
     * @param sentence List of Strings that should be converted
     * @return List  as a single string.
     */

    private String cutToString(List<String> sentence)
    {
        StringBuilder temp = new StringBuilder();
        for (int i = 0 ; i < sentence.size() ; i++)
        {
            temp.append(sentence.get(i));
        }
        return temp.toString();
    }

    /**
     * Method to check whether a file is empty. If the file is empty the method throws an IO Exception to the reading-method where the IO Exception is caught. Otherwise it will be signaled
     * that file is not empty.
     * @param data String that is relative file path.
     * @throws IOException Exception will be handled at readSingleCharacter-method.
     */

    private void checkForEmptyFile (String data) throws IOException
    {
        File test = new File(data);
        if (test.length()==0)
        {
            throw new IOException("Datei ist leer.");
        }

        else
        {
            System.out.println("Datei ist nicht leer.");
        }

    }

    public List<String> characterToString(List<Character> input)
    { List<String> newList2 = new ArrayList<>();

        for (int i = 0; i < input.size(); i++)
        {
            newList2.add(String.valueOf(input.get(i)));
        }

        return newList2;
    }

    public List<String> getTokenlist()
    {
        return this.tokenlist;
    }


}
