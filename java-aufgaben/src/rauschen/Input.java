package rauschen;

import java.util.Scanner;

public class Input
{
    /**
     * Userinput for the file that should be read. Entered als relative path: filename.extension.
     */
    private String inputPath;

    /*
     * Userinput for the file that should be written. Entered als relative path: filename.extension.
     */
    private String outputPath;

    /**
     * Degree of noise that should be used for scrambling the message. If a "noised" file should be read user should only enter 0. Value is not used.
     */
    private int degreeNoise;

    /**
     * Number of repetitions for the scrambling/descrambling of the message.
     */
    private int nrRepetitions;

    /**
     * Constructor for testing the functionalities of the other classes by ommitting the user inputs.
     * @param manualInPath Path of the file to be read.
     * @param manualOutPath Path of the file to be written.
     * @param manualDegreeNoise Degree of the noise created on the original message.
     * @param manualNrRepetitions Number of the repetitions of the message.
     */

    public Input(String manualInPath,String manualOutPath, int manualDegreeNoise, int manualNrRepetitions)
    {
        this.inputPath=manualInPath;
        this.outputPath=manualOutPath;
        this.degreeNoise=manualDegreeNoise;
        this.nrRepetitions=manualNrRepetitions;
    }

    /**
     * Constructor to read user inputs for the final implementation. Invokes the readInput-method where the functionality of reading user inputs is taking place. If a users enter 0 repetitions
     * the programm will tell the user to state another integer, because if the number of repetitions is zero this would lead to crashing the programm by divion through zero in the
     * computeLengthSequence in Noise-Class.
     */

    public Input()
    {
        readInput();


    }

    /**
     * Method to read the necessary inputs for noising and de-noising the message. In case of de-noising the degree of noise is not necesserary.
     * To avoid multiple implementations of a reading method of this class. Users should in the case of de-noising type 0.
     */

    public void readInput()
    {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Geben sie den Pfad der einzulesenden Datei ein");

        while(myScanner.hasNext()==true)
        {

            inputPath=myScanner.next();
            System.out.println("Geben sie den Pfad der auszugebenden Datei ein");
            outputPath=myScanner.next();

            System.out.println("Geben sie die Stärke des Rauschens ein. Wenn kein Verrauschen gewünscht ist, geben sie die 0 ein");
            degreeNoise=myScanner.nextInt();
            System.out.println("Geben sie die Anzahl an Wiederholungen an");
            nrRepetitions=myScanner.nextInt();

            if (nrRepetitions==0)
            {
                System.out.println("Geben sie eine Anzahl von Wiederholungen an, die ungleich null ist");
                nrRepetitions=myScanner.nextInt();
                break;
            }
            break;


        }

    }



    public String getInputPath() {
        return inputPath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public int getDegreeNoise() {
        return degreeNoise;
    }

    public int getNrRepetitions() {
        return nrRepetitions;
    }
}
