package rauschen;
import java.util.List;

public class Message
{

    /**
     * Text of the message as list of strings.
     */
    private List<String> text;

    /**
     * Standard-constructor of the class for creating message-objects.
     *
     * @param input String that should be "noised".
     */

    public Message(List<String> input)
    {
        this.text = input;
    }

    public List<String> getText()
    {
        return this.text;
    }



}


