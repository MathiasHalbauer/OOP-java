package rauschen;

import java.util.*;

public class Noise
{

    /**
     * List of dictionaries to store the frequencies of signs at each first, second, third etc. position
     */
    public List<HashMap<String, Integer>> listOfDictionaries;

    /**
     * Length of each repetition of the message.
     */
    private int lengthPartString;

    /**
     * Global variable for the amount of noise introduced into the raw message.
     */

    private int degreeNoise;

    /**
     * Number of repetitions of scrambling the message.
     */
    private int repetitions;


    /**
     * Constructor for functional capacity with user inputs. This is the final version of the constructor.
     * @param inputValues Userinput by Input-class.
     * @param myMessage message that should be processed.
     */
    public Noise(Message myMessage, Input inputValues)
    {
        this.degreeNoise= inputValues.getDegreeNoise();
        this.repetitions = inputValues.getNrRepetitions();

        System.out.println(myMessage.getText().size());

        //Laenge einer Teilsequenz berechnen
        lengthPartString = computeLengthSequence(myMessage.getText().size(), inputValues.getNrRepetitions());
        System.out.println(this.lengthPartString);
    }

    /**
     * Constructor for testing without other classes.
     * @param degree amount of noise to be introduced.
     * @param repetitions Number of repetitions.
     */

    public Noise(int degree, int repetitions)
    {
        this.degreeNoise=degree;
        this.repetitions=repetitions;
    }


    /**
     * Method for computing the length of the sequences of the message. Each sequence has the same length. Therefor we need only one time to compute the length.
     * @param textLength overall length of the message
     * @param repetitions number of repetitions of each sequence
     * @return length of the seqences of the message
     */

    private int computeLengthSequence(int textLength, int repetitions)
    {
        if(repetitions!=0)
        {
            return textLength/repetitions;
        }
        return -1;

    }

    /**
     * Method tha adds noise to a given message, by invoking the addNoise method several times (equals number of repetitions
     * @param message List of strings that is the message transmitted
     * @return List of strings that is the noisened message.
     */

    public List<String> scramble(List<String> message)
    {
        String temp = cut(message);
        List<String> collate = new ArrayList<>();
        //StringBuilder sbuilder = new StringBuilder();
        for (int i = 0 ; i < this.repetitions ; i++)
        {
            collate.add(addNoise(temp));
            //sbuilder.append();
        }
        return collate;
        //sbuilder.toString();


    }

    /**
     * Method to denoise a given message
     * @param message List of strings, that is a scrambled message.
     * @return List of strings, that is reconstructed from the scrambled message.
     */

    public List<String> descramble(List<String> message)
    {
        List<String> listOfWords = new ArrayList<String>();
        listOfDictionaries = createDictionaries(lengthPartString);
        traverseMessage(message);

        listOfWords.add(showDecryptedMessage());

        return listOfWords;
    }

    /**
     * Method to create the dictionaries which store the characters per position and their frequency
     * There is a dictionary for each position.
     * @param sequenceLength Length of each sequence that is needed for further computation.
     * @return List of dictionaries (Hashmaps)
     */

    private List<HashMap<String, Integer>> createDictionaries(int sequenceLength)
    {
        List<HashMap<String, Integer>> collector = new ArrayList<>();


        for(int i = 0 ; i < sequenceLength ; i++)

            {
                 collector.add(new HashMap<String, Integer>());

            }
        return collector;

    }

    /**
     * Method to insert the characters into the fitting dictionary and count the appearances of singular characters
     * @param index Position of the letter. Determines into which dictionary of listOfDictionaries the letter should be written into.
     * @param character Letter that should be written as key into the dictionary.
     */

    private void addToDictionary(int index, String character)
    {
        if (listOfDictionaries.get(index).containsKey(character))
        {
            int temp = listOfDictionaries.get(index).get(character);
            listOfDictionaries.get(index).put(character, temp +1);
        }
        else if ( !(listOfDictionaries.get(index).containsKey(character)) )
        {
            listOfDictionaries.get(index).put(character, 1);
        }

    }

    /**
     * Method to loop over the message and collect the chars at each positions. Because the noisy signal is repeated and we want to compare each first,
     * second etc. position of each repetition we use modular arithmetic to assign theses indices.
     * @param message List of strings, that contains the "message".
     */


    private void traverseMessage(List<String> message)
    {

        for (int i = 0 ; i < message.size() ; i++)
        {

            //Modular counter to determine which character gets inserted into which dictionary
            int mod = i % lengthPartString;

            addToDictionary(mod, message.get(i));

        }

    }

    /**
     * Method reads the entries of a dictionary. These consist of characters and their frequency at their position in the string.
     * Method returns the characters with the greatest frequency associated.
     * @param map Dictionary of Type Key: String, Value: Integer
     * @return most frequent character in each dictionary.
     */

    private String getMostFrequentLetter(HashMap<String, Integer> map)
    {
        int max = Collections.max(map.values());
        String result = null;
        for (Map.Entry<String, Integer> key : map.entrySet())
        {
            if (key.getValue()== max)
            {
                result = key.getKey();
            }
        }

        return result;

    }

    /**
     * Method shows and returns the decrypted message.
     * @return decrypted message as single string.
     */

    public String showDecryptedMessage()
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < listOfDictionaries.size() ; i++)
        {
            sb.append(getMostFrequentLetter(listOfDictionaries.get(i)));
        }

        System.out.println(sb);

        return sb.toString();
    }

    /**
     * Methode to enricha input string with noise. For task 1 should be executed 6 times.
     * @param inputSentence String  that should be "noisened"
     * @return string with noise.
     */
    private String addNoise (String inputSentence)
    {
        //Bestehender Inputstring wird in eine Liste geschrieben, um Zeichen an einzelnen Indexpositionen zu aendern
        ArrayList<Character> str = new ArrayList<>();

        for (int i = 0; i < inputSentence.length(); i++)
        {
            str.add(i,inputSentence.charAt(i));

        }

        HashSet<Integer> usedIndices = new HashSet<>();
        //Schleife wird so lange durchgefuehrt bis die Anzahl an Chars, die geändert wurden alle geaendert wurden

        int NumberOfCharsToBeChanged = ( inputSentence.length() * this.degreeNoise ) /100;

      for (int i=0; i< NumberOfCharsToBeChanged;i++)
        {
            Random newRandom = new Random();
            int k = newRandom.nextInt( inputSentence.length()-1 - 0 + 1);   //Zufallszahl fuer den Indexzugriff


            /**Abfrage ob der Char zu dem Index schon geaendert wurde.
               Wenn nicht dann zufälligen char in diesen Index einfügen
               Wenn der Char in dem Index schon geändert wurde, dann neue Zufallszahl erstllen und neuen Index suchen

             */
            if (!usedIndices.contains(k))
            {
                Random randAlphabet = new Random();
                int l = randAlphabet.nextInt( 126 - 33 + 1)+ 33;   //zufälliges ELement aus dem Alphabet nehmen
                char insert = (char) l;
                usedIndices.add(k);
                str.set(k,insert);
               // sentence.add(k,alphabet[l]);

            }

            else
            {
                //Fall 2: Index ist enthalten. Neue Zufallszahl generieren
                //neuen Index generieren
                Random nRandom = new Random();
                int p = nRandom.nextInt( inputSentence.length() - 0 + 1);

                //Ersatzchar per Zufall generieren
                int m = newRandom.nextInt(127 - 33 + 1 )+33;

                char insert = (char) m;
                str.set(k,insert);
            }

        }

        return charListToString(str);
    }



    /**
     * Method to convert the IO-Output of a char List into a single string to add Noise with the Methods
     * @param input List of characters that is read by the IO-Class
     * @return concatenates these characters into a single string.
     */
    private String charListToString(ArrayList<Character> input)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < input.size(); i++)
        {
            sb.append(input.get(i));
        }

        return sb.toString();

    }

    /**
     * Converts a list of strings in one string for easier processing
     * @param sentence Input for further processing.
     * @return Input converted into one string.
     */

    

    private String cut(List<String> sentence)
    {
        StringBuilder temp = new StringBuilder();
        for (int i = 0 ; i < sentence.size() ; i++)
        {
            temp.append(sentence.get(i));
        }
        return temp.toString();
    }
}
