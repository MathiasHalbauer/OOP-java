package schmetterlingssimulation;

/**
 * Enum zur Definition des Wertebereichs der Form des Fuehlers: Kann nur die Werte gerade oder gekruemmt annehmen. Andere Eingaben fuehren zum Werfen einer Exception.
 */
public enum fuehlerForm {
    gerade, gekruemmt
}
