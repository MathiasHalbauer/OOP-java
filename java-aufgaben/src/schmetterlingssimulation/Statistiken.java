package schmetterlingssimulation;
import java.util.ArrayList;

/**
 * Diese Klasse dient zur Berechnung und Ausgabe der prozentualen Verteilung einzelner Kombination von Schmetterlingsmerkmalen.
 */

public class Statistiken
{


    /**
     * Berechnungen der prozentualen Haeufigkeiten der einzelnen Merkmalskombinationen durch Aufrufe der Berechnungsmethode.
     * @param input Liste an Schmetterlingen, worauf die prozentuellen Berechnungen durchgefuehrt werden. Dies stellt die Grundgesamtheit für die Berechnungen dar.
     * @param eingabeWerte Ueber die Konsole eingegebene Werte, die die maximal 8 möglichen verschiedenen Merkmalskombinationen(2 * 2 * 2) ergeben. Bei gleichen Merkmalsauspraegungen
     *                    der Eltern koennen sich auch weniger Kombinationen ergeben. Die Prozente bleiben davon unberuehrt.
     */

    public Statistiken(ArrayList<Schmetterling> input, Konsole eingabeWerte)
    {

        berechneProzente(input, eingabeWerte.getVaterMusterung(), eingabeWerte.getVaterFarbe(), eingabeWerte.getVaterForm()); //reiner Fall: Vater


       berechneProzente(input, eingabeWerte.getMutterMusterung(), eingabeWerte.getMutterFarbe(), eingabeWerte.getMutterForm()); //reiner Fall: Mutter


       berechneProzente(input, eingabeWerte.getVaterMusterung(), eingabeWerte.getVaterFarbe(), eingabeWerte.getMutterForm());


        berechneProzente(input, eingabeWerte.getVaterMusterung(), eingabeWerte.getMutterFarbe(), eingabeWerte.getMutterForm());


        berechneProzente(input, eingabeWerte.getMutterMusterung(), eingabeWerte.getMutterFarbe(), eingabeWerte.getVaterForm());


        berechneProzente(input, eingabeWerte.getMutterMusterung(), eingabeWerte.getVaterFarbe(), eingabeWerte.getVaterForm());

        berechneProzente(input, eingabeWerte.getVaterMusterung(), eingabeWerte.getMutterFarbe(), eingabeWerte.getVaterForm());

        berechneProzente(input, eingabeWerte.getMutterMusterung(), eingabeWerte.getVaterFarbe(), eingabeWerte.getMutterForm());

    }


    /**
     * Methode zur Berechnung der Prozente der Kombination von Merkmalen, die dieser Methode als Parameter uebergeben wird.
     * @param stichprobe Liste aufgrund der die prozentualen Anteile berechnet werden.
     * @param muster Musterung der Schmetterlinge
     * @param farbe Farbe der Schmetterlinge
     * @param form Form des Fuehlers der Schmetterlinge
     * @return Prozentualer Anteil dieser Merkmalskombination.
     */

    public Double berechneProzente(ArrayList<Schmetterling> stichprobe ,String muster, String farbe, String form)
    {
        //Den Fall abfangen, dass das gesuchte Merkmal des Wissenschaftlers nicht in der Population vorkommt
        if (stichprobe.isEmpty())
        {
            System.out.println("Keine Berechnung moeglich, da die Liste leer ist");
            return null;
        }

        else
            {
            ArrayList<Schmetterling> aufSummieren = new ArrayList<>();
            for (int i = 0; i < stichprobe.size() - 1; i++)
            {
                if (stichprobe.get(i).getMusterung() == muster && (stichprobe.get(i).getFluegelFarbe() == farbe && stichprobe.get(i).getFuehlerform() == form))
                {
                    aufSummieren.add(stichprobe.get(i));
                }
            }
            System.out.println("Die Merkmalskombination: " + muster + ", " + farbe + ", " + form + " hat " + (aufSummieren.size() * 100) / stichprobe.size() + " Prozent.");
            return (double) ((aufSummieren.size() * 100.00) / stichprobe.size());


        }




    }





}
