package schmetterlingssimulation;
/**
 * Enum zur Definition des Wertebereichs der Form der Fluegel: Kann nur die Werte rot, gelb, gruen oder blau annehmen. Andere Eingaben fuehren zum Werfen einer Exception.
 */
public enum fluegelFarbe {
    rot, gelb, gruen, blau
}
