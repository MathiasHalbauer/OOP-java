package schmetterlingssimulation;
import java.util.ArrayList;

/**
 * Diese Klasse stellt den Wissenschaftler dar, der die Schmetterlinge mit einem vom User gewuenschten Merkmal sammelt.
 */

public class Wissenschaftler
{
    /**
     * Gesammelte Schmetterlinge des Wissenschaftlers werden in dieser Liste gespeichert.
     */
    private ArrayList<Schmetterling> sammeltrommel;

    /** Parametrisierter Konstruktor zur Uebernahme von Informationen, die benoetigt werden. Parameter werden in sammleSchmetterling weitergereicht
     *
     * @param zuFangen Gesamtpopulation von Schmetterlingen aus der eine bestimmte Art von Schmetterling gefangen werden soll
     * @param eingabeWerte Werte, die die Konsole aufnimmt. Dient zur Bestimmung des Merkmals, dass der Wissenschaftler bei den Schmetterlingen sucht.
     */
    public Wissenschaftler(ArrayList<Schmetterling> zuFangen, Konsole eingabeWerte)
    {
        sammeltrommel =sammleSchmetterling(zuFangen,eingabeWerte.getGesuchtesMerkmal());
    }

    /**Methode zum "Fangen" der Schmetterlinge
     *
     * @param zuFangen Gesamtpopulation von Schmetterlingen aus der eine bestimmte Art von Schmetterling gefangen werden soll
     * @param gesuchtesMerkmal Wert zur Auswahl welche Schmetterlinge gefangen werden sollen
     */

    public ArrayList<Schmetterling> sammleSchmetterling(ArrayList<Schmetterling> zuFangen, String gesuchtesMerkmal)
    {
        ArrayList<Schmetterling> sammler = new ArrayList<>();
        //Suche in Liste nach Schmetterlingen mit diesen Eigenschaften
        for (int i=0; i < zuFangen.size()-1;i++)
        {
            if (zuFangen.get(i).getMusterung() ==gesuchtesMerkmal || zuFangen.get(i).getFluegelFarbe()==gesuchtesMerkmal || zuFangen.get(i).getFuehlerform()==gesuchtesMerkmal )
            {
                sammler.add(zuFangen.get(i));
            }
        }
        return sammler;
    }

    /**
     * Zugriffsmethode für die Gesamtheit der gespeicherten Schmetterlinge.
     * @return Liste der gespeicherten Schmetterlinge
     */
    public ArrayList<Schmetterling> getSammeltrommel()
    {
        return sammeltrommel;
    }

}
