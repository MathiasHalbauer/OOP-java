package schmetterlingssimulation;

/**
 * Enum zur Definition des Wertebereichs der Muster: Kann nur die Werte kein Muster oder schwarze Punkte annehmen. Andere Eingaben fuehren zum Werfen einer Exception.
 */
public enum musterung {
    keinMuster, schwarzePunkte, schwarzeStreifen
}
