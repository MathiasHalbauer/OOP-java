package schmetterlingssimulation;
import java.util.ArrayList;
import java.util.Random;

/**
 * Diese Klasse dient dazu einzelne Schmetterlinge zu erstellen. In getrennten Methoden werden die Eltern, sowie die Kinder mit den vererbten Merkmalen erstellt.
 */
public class SchmetterlingsSammlung
{
    /**
     * Die Gesamtheit der erstellten Schmetterlinge wird als Liste innerhalb der Klasse Schmetterlingssammmlung gespeichert.
     */
    private ArrayList<Schmetterling> schmetterlingsPopulation;


    /**
     * Konstruktor uebernimmt die Daten von der Konsole und ruf damit die Methode zur Erstellung der Eltern auf und
     * generiert ein Vater- und ein Mutterexemplar und ruft dann die Methode zur Erstellung der Kinder aus.
     * @param eingabeDaten Instanz der Konsolen-Klasse mit gewuenschten Elternmerkmalen.
     */
    public SchmetterlingsSammlung(Konsole eingabeDaten)
    {

     Schmetterling vater  = erstelleEltern(eingabeDaten.getVaterMusterung(), eingabeDaten.getVaterFarbe(), eingabeDaten.getVaterForm());
     Schmetterling mutter = erstelleEltern(eingabeDaten.getMutterMusterung(), eingabeDaten.getMutterFarbe(), eingabeDaten.getMutterForm());
     this.schmetterlingsPopulation =   erstelleKinder(vater, mutter);

    }

    /**
     * Methode zum externen Zugriff auf die Gesamtheit der erstellten Schmetterlinge. Dies wird fuer den Wissenschaftler
     * und die statistische Verarbeitung benoetigt.
     * @return Gibt eine Liste von Schmetterlingen zurueck.
     */
    public ArrayList<Schmetterling> getSchmetterlingsPopulation()
    {
        return schmetterlingsPopulation;
    }

    /**
     * Erstellung des Vater- und Mutterschmetterlings nach den Eingaben des Users.
     * @param muster Gewuenschtes Muster des Elternteils.
     * @param farbe Gewuenschte Farbe des Elternteils.
     * @param form Gewuenschte Form des Elternsteils.
     * @return Schmetterlingsobjekt(je ein Vater und je eine Mutter)
     */

    public Schmetterling erstelleEltern(String muster, String farbe,  String form)
    {
        return new Schmetterling(muster, farbe, form);
    }

    /**
     * Erstellung der Kinder mit k (zufallsgenerierter) Anzahl
     * @param vater Vaterschmetterling
     * @param mutter Mutterschmetterling
     * @return Kindschmetterling, der die Merkmale seiner Eltern zufaellig erbt
     */

    public ArrayList<Schmetterling> erstelleKinder(Schmetterling vater, Schmetterling mutter)
    {

        System.out.println("Ausgabe der Vater-Merkmale:\n");
        System.out.println(vater + "\n");

        System.out.println("Ausgabe der Mutter-Merkmale:\n");

        System.out.println(mutter);
        System.out.println("-------------------------------------------------------------------------------------------");



        //Logik der zufallsmäßigen Erstellung der Kinder hier unter Maßgabe der importierten Eltern

        ArrayList<Schmetterling> schmetterlingsPopoulation = new ArrayList<>();

        Random r = new Random();
        int k = r.nextInt(2000- 1000 + 1) +1000;

        System.out.println("Es werden " + k + " Kinder erstellt.");

        for (int i=0; i <k; i++)
        {
            schmetterlingsPopoulation. add(new Schmetterling(zufaelligesMuster(vater, mutter), zufaelligeFarbe(vater, mutter), zufaelligeForm(vater, mutter)));
        }

        return schmetterlingsPopoulation;
    }

    /**
     * Methode zur Vererbung des Musters der Eltern.
     * @param vater Vaterschmetterling
     * @param mutter Mutterschmetterling
     * @return Merkmalsauspraegung, die vererbt wird
     */
    private String zufaelligesMuster(Schmetterling vater, Schmetterling mutter)
    {
        String[] muster= new String[2];
        muster[0] = vater.getMusterung();
        muster[1] = mutter.getMusterung();

        Random r = new Random();
        int x = r.nextInt(1- 0 + 1) +0;

        return muster[x];
    }
    /**
     * Methode zur Vererbung der Farbe der Eltern.
     * @param vater Vaterschmetterling
     * @param mutter Mutterschmetterling
     * @return Merkmalsauspraegung, die vererbt wird
     */

    private String zufaelligeFarbe(Schmetterling vater, Schmetterling mutter)
    {
        String[] farbe= new String[2];
        farbe[0] =vater.getFluegelFarbe();
        farbe[1] = mutter.getFluegelFarbe();

        Random r = new Random();
        int x = r.nextInt(1- 0 + 1) +0;

        return farbe[x];
    }

    /**
     * Methode zur Vererbung des Form der Fluegel der Eltern.
     * @param vater Vaterschmetterling
     * @param mutter Mutterschmetterling
     * @return Merkmalsauspraegung, die vererbt wird
     */

    private String zufaelligeForm(Schmetterling vater, Schmetterling mutter)
    {
        String[] form= new String[2];
        form[0] =vater.getFuehlerform();
        form[1] = mutter.getFuehlerform();

        Random r = new Random();
        int x = r.nextInt(1- 0 + 1) +0;

        return form[x];
    }

}
