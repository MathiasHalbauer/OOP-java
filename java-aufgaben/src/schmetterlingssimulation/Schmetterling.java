package schmetterlingssimulation;

/**
 * Diese Klasse stellt den Bauplan eines Schmetterlingsexemplar dar.
 */
public class Schmetterling
{
    /**
     * Musterung des Schmetterlingssexemplar.
     */
    private String musterung;
    /**
     * Farbe der Fluegel des Schmetterlingsexemplar.
     */
    private String fluegelFarbe;
    /**
     * Form des Fuehlers des  Schmetterlingssexemplar.
     */
    private String fuehlerform;

    /**Parametrisierter Konstruktor zur Erstellung der Schmetterling in der Schmetterlingssammlung.
     *
     * @param muster Musterung des Schmetterlings(kein Muster, schwarze Punkte, schwarze Steifen
     * @param farbe Flügelfarbe des Schmetterlings(Rot, gelb, grün, blau)
     * @param form Fühlerform des Schmetterlings(gerade, gekrümmt)
     */
    public Schmetterling(String muster, String farbe, String form)
    {
           this.musterung=muster;
           this.fluegelFarbe=farbe;
           this.fuehlerform=form;
           printProperties();

    }

    /**
     * Alternativer Konstruktor für einfachen Testfall. Prüfung der Verbindung der Konsole mit der Schmetterlingsklasse.
     * @param konsole Eingabewerte aus Konsole für Erstellung eines Testschmetterlings mit diesen Merkmalen.
     */

    public Schmetterling(Konsole konsole)
    {
        this.musterung=konsole.getVaterMusterung();
        this.fluegelFarbe=konsole.getVaterFarbe();
        this.fuehlerform= konsole.getVaterForm();
    }

    /**
     * Ausgabe der Eigenschaften des jeweiligen Schmetterlings auf der Konsole.
     *  */
    public void printProperties()
    {
        System.out.println("Dieser Schmetterling hat " + this.musterung + ", seine Flügelfarbe ist " + this.fluegelFarbe + " und er hat einen " + this.fuehlerform +"en Fühler.");
    }

    /**
     * Zugriffsmethode fuer das Muster der Schmetterlinge
     * @return Musterung wird als String zurueckgegeben
     */

    public String getMusterung()
    {
        return musterung;
    }

    /**
     * Zugriffsmethode für die Farbe der Fluegel
     * @return Fluegelfarbe wird als String zurueckgegeben
     */

    public String getFluegelFarbe()
    {
        return fluegelFarbe;
    }

    /**
     * Zugriffsmethode für die Form der Fuehler.
     * @return Form der Fuehler wird als String zurueckgegeben.
     */

    public String getFuehlerform()
    {
        return fuehlerform;
    }
}
