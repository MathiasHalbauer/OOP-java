package schmetterlingssimulation;

/**
 * Enum zur Definition des Wertebereichs des gesuchten Merkmals. Es wird sichergestellt, dass nur moegliche Merkmalsauspraegungen eingegeben werden. Dies ist somit die Vereinigungsmenge
 * der einzelnen Wertebereiche fuer die Merkmale Form, Farbe, Musterung. Andere Eingaben fuehren zum Werfen einer Exception.
 */
public enum gesuchtesMerkmal {
    keinMuster, schwarzePunkte, schwarzeStreifen, rot, gelb, gruen, blau, gerade, gekruemmt
}
