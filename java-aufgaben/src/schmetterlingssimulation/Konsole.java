package schmetterlingssimulation;
import java.util.Scanner;

/**
 * Klasse zur Verarbeitung der Usereingabe mittels Konsole.
 */
public class Konsole
{


    /**
     * Instanzvariable: vom Wissenschaftler gesuchtes Merkmal, das eingegeben wurde
     */
    private gesuchtesMerkmal gesucht;

    /**
     * Instanzvariable: Muster des Vaters, welches eingegeben wurde.
     */
    private musterung vaterMusterung;

    /**
     * Instanzvariable: Mutter der Musterung, die eingegeben wurde.
     */
    private musterung mutterMusterung;

    /**
     * Instanzvariable: Farbe des Vaters, die eingegeben wurde.
     */
    private fluegelFarbe vaterFarbe;

    /**
     * Instanzvariable: Farbe der Mutter, die eingegeben wurde.
     */
    private fluegelFarbe mutterFarbe;

    /**
     * Instanzvariable: Form des Fuehlers des Vaters, die eingegeben wurde.
     */
    private fuehlerForm vaterForm;
    /**
     * Instanzvariable: Form des Fuehlers der Mutter, die eingegeben wurde.
     */
    private fuehlerForm mutterForm;


    /** Konstruktor fuer Erfassung der Eingaben. Mit Hilfe eines Scanners werden die gesuchten Werte hintereinander als Strings eingegeben. Merkmalsauspraegungen, die nicht der Aufgaben-
     * stellung entsprechen fuehren zum Werfen einer Exception. Die Werte werden als Instanzvariablen gespeichert und den anderen Klassen uebergeben.
     *
     */
    public Konsole()
    {

        System.out.println("Erwarte Eingabe");
        Scanner myScanner= new Scanner(System.in);
        System.out.println("Geben sie das gesuchte Merkmal ein");

        while(myScanner.hasNext()==true)
        {



            //System.out.println(myScanner.match());
                /*
                if (myScanner.hasNext(h.contains(myScanner.hasNext()))) //kann einfache Werte abfangen
                {
                    */
                this.gesucht =gesuchtesMerkmal.valueOf(myScanner.next());
                System.out.println("Geben sie das gewünschte Muster/Punktierung des Vaters ein"); //aktuell kein Leerzeichen verwenden: entweder 2 Eingaben oder den String später wieder trennen
                this.vaterMusterung = musterung.valueOf(myScanner.next());
                System.out.println("Geben sie das gewünschte Muster/Punktierung der Mutter ein");
                this.mutterMusterung= musterung.valueOf(myScanner.next());
                System.out.println("Geben sie die gewünschte farbe des Vaters ein");
                this.vaterFarbe= fluegelFarbe.valueOf(myScanner.next());
                System.out.println("Geben sie die gewünschte farbe der Mutter ein");
                this.mutterFarbe= fluegelFarbe.valueOf(myScanner.next());
                System.out.println("Geben sie die gewünschte Fühlerform des Vaters ein");
                this.vaterForm= fuehlerForm.valueOf(myScanner.next());
                System.out.println("Geben sie die gewünschte Fühlerform der Mutter ein");
                this.mutterForm= fuehlerForm.valueOf(myScanner.next());
                System.out.println("Eingabe beendet");
                break;
            }
    }

    /**
     * Konstruktor zum Testen der Programmfunktionalitaet ohne jedesmal die Werte neu eingeben zu müssen
     * @param gesucht Merkmal, das vom Wissenschaftler gesucht werden soll.
     * @param vater1 Musterung des Vaters.
     * @param mutter1 Musterung der Mutter.
     * @param vater2 Farbe des Vaters.
     * @param mutter2 Farbe der Mutter.
     * @param vater3 Fuehlerform des Vaters.
     * @param mutter3 Fuehlerform der Mutter.
     */

    public Konsole(gesuchtesMerkmal gesucht, musterung vater1, musterung mutter1, fluegelFarbe vater2, fluegelFarbe mutter2, fuehlerForm vater3, fuehlerForm mutter3)
    {
        this.gesucht=gesucht;
        this.vaterMusterung=vater1;
        this.mutterMusterung=mutter1;
        this.vaterFarbe=vater2;
        this.mutterFarbe=mutter2;
        this.vaterForm=vater3;
        this.mutterForm=mutter3;

    }

    public void test()
    {
        System.out.println(gesucht);
        System.out.println(vaterMusterung);
        System.out.println(mutterMusterung);
        System.out.println(vaterFarbe);
        System.out.println(mutterFarbe);
        System.out.println(vaterForm);
        System.out.println(mutterForm);
    }

    /**
     * Zugriffsmethode fuer die vom Wissenschaftler gesuchte Schmetterlingsmerkmalsauspraegung
     * @return Diese wird als String zurueckgegeben.
     */

    public String getGesuchtesMerkmal()
    {
        return gesucht.toString();
    }

    /**
     * Zugriffsmethode fuer die Musterung, die der Vater erhalten soll
     * @return Diese wird als String zurueckgegeben.
     */

    public String getVaterMusterung()
    {
        return vaterMusterung.toString();
    }

    /**
     * Zugriffsmethode fuer die Musterung, die die Mutter erhalten soll
     * @return Diese wird als String zurueckgegeben.
     */

    public String getMutterMusterung()
    {
        return mutterMusterung.toString();
    }

    /**
     * Zugriffsmethode fuer die Farbe, die der Vater erhalten soll
     * @return Diese wird als String zurueckgegeben.
     */

    public String getVaterFarbe()
    {
        return vaterFarbe.toString();
    }

    /**
     * Zugriffsmethode fuer die Farbe, die die Mutter erhalten soll
     * @return Diese wird als String zurueckgegeben.
     */


    public String getMutterFarbe()
    {
        return mutterFarbe.toString();
    }

    /**
     * Zugriffsmethode fuer die Form des Fuehlers, die der Vater erhalten soll
     * @return Diese wird als String zurueckgegeben.
     */


    public String getVaterForm()
    {
        return vaterForm.toString();
    }

    /**
     * Zugriffsmethode fuer die Form des Fuehlers, die die Mutter erhalten soll
     * @return Diese wird als String zurueckgegeben.
     */

    public String getMutterForm()
    {
        return mutterForm.toString();
    }




}
