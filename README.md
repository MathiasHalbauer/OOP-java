# Übungsaufgaben Praktikum Objektorientierte Programmierung

Sammlung von Aufgaben zu objektorientierter Programmierung mit **Java SE 8.0**

# 1 Schmetterlingssimulation (Collections)

In Mendel’s Land gibt es eine fantastische Vielfalt von Schmetterlingen. Man sieht welche mit roten,
schwarz gepunkteten Flügeln und gekrümmten Fühlern, andere sind schwarz gelb gestreift und haben
gerade Fühler, usw.

Das Programm führt folgendes durch:

1. zufällig 1000 ≤ k ≤ 2000 Kinder entsprechend der Vererbungsregeln erzeugt,
2. deren Merkmale vergleichen kann,
3. alle Kinder mit einem festgelegten Merkmal m ermittelt und
4. diese Kinder mit Merkmal m in eine Sammeltrommel legt.

# 2 Verrauschen von Nachrichten (Streams)

Schon seit Jahren ist die Raumsonde Enaira im Weltall unterwegs. Da die Entfernung zur Erde mittlerweile sehr gross ist, werden ihre Nachrichten auf der Erde nur noch verrauscht empfangen. Aus diesem
Grunde schickt Enaira jede Nachricht mehrmals los. Die Anzahl der Wiederholungen einer jeden Nach-
richt wurde vor dem Start der Raumsonde vereinbart. Aus der Serie von Nachrichten kann nun auf der
Erde die Originalnachricht ermittelt werden.

Schreiben Sie ein Programm, welches Nachrichten im ASCII-Format ver- und entrauscht.
Eine Nachricht wird mehrmals verrauscht und die Sequenz der verrauschten Nachrichten wird in eine Datei geschrieben. Als Eingabe dienen die Originalnachricht, die Anzahl der
Wiederholungen und die Stärke des Rauschens im Verhältnis zur Länge der Eingabe in Prozent.
40% Rauschen bedeuten bei einer Nachrichtenlänge von 20 Zeichen, dass 8 zufällig ausgewählte
Positionen mit zufällig gewählten Zeichen belegt werden. Die Sequenz von verrauschten Nachrichten wird aus einer Datei eingelesen und eine
mögliche entrauschte Nachricht ausgegeben.

1. Erzeugen Sie mittels Ihres Programmes einen verrauschten Text und entrauschen Sie ihn anschliessend. Verwenden Sie hierzu den Text "praktikum", 6 Wiederholungen und ein Rauschen
von 70%.
2. Entrauschen Sie die Nachricht aus der Datei ‘Nachricht1.txt’ (erzeugt mit 6 Wiederholungen und
70% Rauschen).
3. Entrauschen Sie die Nachricht aus der Datei ‘Nachricht2.txt’ (erzeugt mit 11 Wiederholungen
und 90% Rauschen).

# 3 Aquarium (Rekursion)

Wer ein Aquarium mit exotischen Fischen einrichten will, kommt nicht ohne den Rat des Experten
aus. Wer ausserdem bei beschränktem Geldbeutel versucht, Fische möglichst vieler verschiedener Arten
gemeinsam unterzubringen, fragt besser gleich den Computer. Der Grund ist einfach: Nicht jede Art
verträgt sich mit jeder anderen und natürlich sollten keine Fische unverträglicher Arten zusammen im
Aquarium leben und sich bekämpfen oder gar - bei Fischen eine durchaus gängige Methode - gegenseitig
auffressen.

Schreiben Sie ein Programm, welches Fische, ihre Verträglichkeit und ihre Preise verwaltet und rekursiv
eine maximale Anzahl verschiedener verträglicher Fischarten zu einem vorgegebenen Preis für das
Aquarium zusammenstellt.

Testen Sie Ihr Programm mit dem folgenden Beispiel, einer Summe von 170,- Euro sowie mehreren
zusätzlichen Geldbeträgen.  

| Fisch | Preis | Unverträglich mit  |
| :----------- | :----------- |:----------- |
| Grüne Migräne | 70,- Euro | Breitmaulmolch, Grottensprotte|
| Korallenqualle | 50,- Euro | |
| Schuppenschatulle | 30,- Euro | Breitmaulmolch, Prachtpiranha|
| Breitmaulmolch | 40,- Euro | Grüne Migräne, Schuppenschatulle|
| Prachtpiranha | 40,- Euro | Schuppenschatulle, Grottensprotte|
| Zitterling | 30,- Euro | Grottensprotte|
| Grottensprotte | 20,- Euro | Grüne Migräne, Prachtpiranha, Zitterling|